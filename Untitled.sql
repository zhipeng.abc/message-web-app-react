create database message_web_app_db;
use message_web_app_db;
select * from member;
select * from picture;
select * from message;

drop database message_web_app_db;
drop table member;
drop table picture;
drop table message;

create table member(
	member_id int not null,
    password varchar(256) not null,
    member_name varchar(256) not null,
    primary key(member_id)
);


select  receiver_id as friend from message where sender_id = 123 union select sender_id as friend from message where receiver_id = 123; 




create table picture(
	url varchar(256) not null,
    member_id int not null,
    primary key(url),
    foreign key(member_id) references member(member_id)
);


create table message(
	message_id int not null auto_increment,
    message_time datetime not null,
    content varchar(256),
    sender_id int not null,
    receiver_id int not null,
    conversation_id varchar(256) not null,
    primary key(message_id),
    foreign key(sender_id) references member(member_id),
	foreign key(receiver_id) references member(member_id)
);
select * from message where (sender_id = 1111 and receiver_id = 3333) OR (receiver_id = 1111 and sender_id = 3333);
select * from message where conversation_id = "1111_3333";


select * from message order by message_id desc;
select * from message m where exists(select max(message_id) as last_message_id from message where sender_id = 1111 or receiver_id = 1111 group by conversation_id having last_message_id=m.message_id  );
select * from message join member on member.member_id=message.receiver_id where exists(select max(message_id) as last_message_id from message where sender_id = 2222 or receiver_id = 2222  group by conversation_id having last_message_id=message_id);