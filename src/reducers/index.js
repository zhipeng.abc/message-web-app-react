import { combineReducers } from "redux";

const webAppInitialState = {
    memberId: "",
    token: "",
    isLogin: false,
    friendList: [],
    loginSuccessMessage: "",
    loginFailMessage: "",
    registerSuccessMessage: "",
    registerFailMessage: "",
    // fetchListFailMessage: "",
    foundMember: [],
    foundMemberSuccessMessage: "",
    foundMemberFailMessage: "",
    conversationsFromServer: [],
    fetchConversationSuccessMessage: "",
    fetchConversationFailMessage: "",
    sendMessageFail:"",
    senderIdForConversationContent: "",
    receiverIdForConversationContent: "",
    socket:null,
    turnToRegister: false,
}

const webApp = function (state = webAppInitialState, action){
    switch(action.type){ 
        case "LOGIN_SUCCESS":
            return {
                ...state,
                token: action.payload.token,
                loginSuccessMessage: action.payload.notification,
                isLogged: true,
                memberId: action.payload.memberId
            }
        case "LOGIN_FAIL":
            return {
                ...state,
                token: "",
                loginFailMessage: action.payload.notification,
                isLogin: false,
            }
        case "REGISTER_SUCCESS":
            return {
                ...state,
                isLogin: true,
                token: action.payload.token,
                registerSuccessMessage: action.payload.notification,
            }
        case"REGISTER_FAIL":
            return{
                ...state,
                registerFailMessage: action.payload.notification,
                isLogged: false,
            }
        case "FETCH_LIST_SUCCESS":
            return{
                ...state,
                friendList: action.payload.friendList
            }
        case "FETCH_LIST_FAIL":
            return{
                ...state,
                fetchListFailMessage: action.payload.message
            }
        case "LOGOUT":
            return{
                ...state,
                isLogin: false,
                token: "",
                friendList: [],
                memberId: "",
                conversationsFromServer: []
            }
        case "SEARCH_MEMBER_SUCCESS":
            return {
                ...state,
                foundMember: action.payload.member,
                foundMemberFailMessage: "",
                foundMemberSuccessMessage: action.payload.message
            }
        case "SEARCH_MEMBER_FAIL":
            return {
                ...state,
                foundMember: [],
                foundMemberFailMessage: action.payload.message,
                foundMemberSuccessMessage: ""
            }
        case "FETCH_CONVERSATIONS_SUCCESS":
            return {
                ...state,
                conversationsFromServer: action.payload.conversations,
                fetchConversationSuccess: action.payload.message,
                fetchConversationFail: "",
                senderIdForConversationContent: action.payload.senderId,
                receiverIdForConversationContent: action.payload.receiverId,
            }
        case "FETCH_CONVERSATION_FAIL":
            return{
                ...state,
                fetchConversationSuccessMessage: "",
                fetchConversationFailMessage: action.payload.message
            }
        case "RECEIVE_SUCCESS":
        case "SEND_SUCCESS":
            return{
                ...state,
                conversationsFromServer: [...state.conversationsFromServer, action.payload.messageDetail],
                sendMessageFail: "",
                // senderIdForConversationContent: action.payload.memberId,
                // receiverIdForConversationContent: action.payload.member2Id,
            }
            case"SEND_FAIL":
            return{
                ...state,
                sendMessageFail:action.payload.message,
            }
        case "ADD_FRIEND_SUCCESS":
            return{
                ...state,
                conversationsFromServer: [...state.conversationsFromServer, action.payload.messageDetail],
                sendMessageFail: ""
            }
        case "SOCKET_CONNECTED":
            return{
                ...state,
                socket: action.payload
            }
        case "TURN_TO_REGISTER":
            return{
                ...state,
                turnToRegister: true
            }
        case "CLEAN_CONVERSATION":
            return{
                ...state,
                conversationsFromServer: []
            }
        case "FIRST_SEND":
            return{
                ...state,
                senderIdForConversationContent: action.senderId,
                receiverIdForConversationContent: action.receiverId,
          
            }
        default: 
            return state;
    }
}

const rootReducer = combineReducers({
    webApp : webApp
})

export default rootReducer;