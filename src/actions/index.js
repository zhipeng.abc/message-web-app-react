import { io } from "socket.io-client";
const endpoint = "http://cicdbackend-env.eba-vghzihpa.ap-southeast-1.elasticbeanstalk.com";
export const submitLogin = function (inputMemberId, inputPassword) {
    return function (dispatch, getState) {
        fetch(endpoint+"/login", {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                memberId: inputMemberId.toString(),
                password: inputPassword.toString()
            })
        }).then((res) => {
            return res.json();
        }).then((resData) => {
            if (resData.success) {
                dispatch({
                    type: "LOGIN_SUCCESS",
                    payload: {
                        token: resData.token,
                        notification: resData.message,
                        memberId: inputMemberId
                    }
                })
                
            } else {
                dispatch({
                    type: "LOGIN_FAIL",
                    payload: {
                        notification: resData.message
                    }
                })
            }
        })
    }
}

export const saveMySocket = function(memberId){
    return function(dispatch, getState){
        getState().webApp.socket.emit("saveMySocket",memberId)
    }
}
export const submitRegister = function (inputMemberId, inputPassword) {
    return function (dispatch, getState) {
        fetch(endpoint+"/register", {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                memberId: inputMemberId.toString(),
                password: inputPassword.toString()
            })
        }).then((res) => {
            return res.json();
        }).then((resData) => {
            
            if (resData.success) {
                localStorage.setItem("memberId", inputMemberId)
                dispatch({
                    type: "REGISTER_SUCCESS",
                    payload: {
                        token: resData.token,
                        notification: resData.message
                    }
                });
                
            } else {
                dispatch({
                    type: "REGISTER_FAIL",
                    payload: {
                        notification: resData.message
                    }
                })
            }
        })
    }
}

export const fetchList = function (token) {
    return function (dispatch, getState) {
        fetch(endpoint+"/member/friendList", {
            method: "GET",
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${token}`,
            },
        }).then((res) => {
            return res.json()
        }).then((resData) => {
            if (resData.success) {
                dispatch({
                    type: "FETCH_LIST_SUCCESS",
                    payload: {
                        friendList: resData.friendList
                    }
                })
            } else {
                dispatch({
                    type: "FETCH_LIST_FAIL",
                    payload: {
                        message: resData.message
                    }
                })
            }
        })
    }
}

export const submitLogout = function () {
    return function (dispatch, getState) {
        dispatch({
            type: "LOGOUT"
        })
    }
}

export const searchMember = function (inputMemberId) {
    return function (dispatch, getState) {
        fetch(endpoint+`/search?memberId=${inputMemberId}`)
            .then((res) => {
                return res.json();
            }).then((resData) => {
                if (resData.success) {
                    dispatch({
                        type: "SEARCH_MEMBER_SUCCESS",
                        payload: {
                            message: resData.message,
                            member: resData.member
                        }
                    })
                } else {
                    dispatch({
                        type: "SEARCH_MEMBER_FAIL",
                        payload: {
                            message: resData.message
                        }
                    })
                }
            })
    }
}

export const submitSendMessage = function(inputText, token, member2Id){
    return function(dispatch, getState){
        fetch(endpoint+`/member/send/${member2Id}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                content: inputText
            })
        }).then((res)=>{
            return res.json();
        }).then((resData)=>{
            if(resData.success){
                dispatch({
                    type: "SEND_SUCCESS",
                    payload: {
                        messageDetail: resData.messageDetail,
                    }
                })
            }else{
                dispatch({
                    type: "SEND_FAIL",
                    payload: {
                        sendMessageFailMessage: resData.message,
                    }
                })
            }
        })
    }
}

export const fetchMessage = function (token, conversationId, senderId, receiverId){
    return function(dispatch, getState){
        fetch(endpoint+`/member/conversation/${conversationId}`, {
            method: "GET",
            headers: {
                "Content-type": "application/json",
                Authorization: `Bearer ${token}`,
            }
        }).then((res)=>{
            return res.json()
        }).then((resData)=>{
            if(resData.success){
                dispatch({
                    type: "FETCH_CONVERSATIONS_SUCCESS",
                    payload: {
                        conversations: resData.conversation,
                        message: resData.message,
                        senderId: senderId,
                        receiverId: receiverId,
                    }
                })
            }else{
                dispatch({
                    type: "FETCH_CONVERSATIONS_FAIL",
                    payload: {
                        message: resData.message
                    }
                })
            }
        })
    }
}

export const submitAddFriend = function(member2Id, token){
    return function(dispatch, getState){
        fetch(endpoint+`/member/send/${member2Id}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                content: "hi"
            })
        }).then((res)=>{
            return res.json();
        }).then((resData)=>{
            if(resData.success){
                dispatch({
                    type: "ADD_FRIEND_SUCCESS",
                    payload: {
                        messageDetail: resData.messageDetail,
                    }
                })
            }else{
                dispatch({
                    type: "ADD_FRIEND_FAIL",
                    payload: {
                        sendMessageFailMessage: resData.message,
                    }
                })
            }
        })
    }
}


export const connectSocket = function(){
    

    return function(dispatch, getState){
        const socket = io(endpoint);
        socket.on("receiveMsg",(msg)=>{
            dispatch(fetchList(localStorage.getItem("token")));
            dispatch({
                type: "RECEIVE_SUCCESS",
                payload: {
                    messageDetail: {
                        messageTime: msg.messageTime,
                        content: msg.content,
                        senderId: msg.senderId,
                    }
                }
            })
        })
        dispatch({
            type:"SOCKET_CONNECTED",
            payload:socket
        })
    } 
}
 

export const sendSocketMessage = function(inputText, token, memberId, member2Id){
    return function(dispatch, getState){
        const socket = getState().webApp.socket;
        socket.emit("sendMessage", {content: inputText, token: token, member2Id: member2Id})
        
        dispatch({
            type: "SEND_SUCCESS",
            payload: {
                messageDetail: {
                    message_time: new Date(),
                    content: inputText,
                    sender_id: memberId,
                    receiver_id: member2Id
                },
                memberId: memberId,
                member2Id: member2Id
            }
        })
    }
}

export const submitTurnToRegister = function(){
    return function(dispatch, getState){
        dispatch({
            type: "TURN_TO_REGISTER"
        })
    }
}

export const clearConversation = function(){
    console.log("here")
    return {
        type: "CLEAN_CONVERSATION"
    }
}

export const firstSend = function(senderId, receiverId){
    return{
        type: "FIRST_SEND",
        senderId, receiverId
    }
}