const express =require("express");
const { comments } = require("./default-data");
const app = express();
const port = 4800;
const db = require("./default-data");
const multer = require('multer');
const upload = multer({dest:'uploaded-files/'});
const cookieParser = require("cookie-parser");

console.log(db);
app.use(express.json());
app.use(cookieParser());

app.get("/fake-li/threads", (req, res)=>{
    let category = parseInt(req.query.category);
    
    let found = db.threads.filter((thread)=>{
        return thread.category === category;
    })
    res.json(found);
})

app.get("/fake-li/threads/:threadId", (req, res)=>{
    let threadId = parseInt(req.params.threadId);
    let found = db.threads.find((thread)=>{
        return thread.id === threadId;
    })
    res.send(found)
})


app.post("/fake-li/comments", (req, res)=>{
    let threadId = req.body.threadId;
    let comment = req.body.comment;
    let authorId = req.body.authorId;

    let newComment = {
        id: Math.floor(Math.random()*100000),
        threadId: threadId,
        content: comment,
        author: authorId
    }
    db.comments.push(newComment);
    res.json({
        "success": true,
        "commentId": newComment.id
    })
})

app.patch("/fake-li/comments/:commentId", (req, res)=>{
    let commentId = parseInt(req.params.commentId);
    let content = req.body.content;
    let found = db.comments.find((comment)=>{
        return comment.id === commentId;
    })
    if(found){
        found.content = content;
        res.json({
            "success": true
        })
        console.log(found)
    }else{
        res.json({
            "success": false
        })
    }
    
})

app.delete("/fake-li/comments/:commentId", (req, res)=>{
    let commentId = parseInt(req.params.commentId);
    let found = db.comments.find((comment)=>{
        return comment.id === commentId;
    })
    if(found){
        db.comments.splice(db.comments.indexOf(found), 1)
        res.json({
            "success": true
        })
        console.log(db.comments)
    }else{
        res.json({
            "success": false
        })
    }
})


app.get("/fake-li/comments/format/json", (req, res)=>{
    res.set("Content-Disposition", "attachment; filename=comments.json");
    res.json(db.comments);
})
 

app.post("/fake-li/image", upload.single("picture"), (req, res)=>{
    let imgName = req.file.originalname;
    let imgPath = req.file.destination;

    res.json({
        "imgName": imgName,
        "imgPath": imgPath
    })
})

app.post("/fake-li/account/login", (req, res)=>{
    let foundAccount = db.accounts.find((account)=>{
        return account.userId === req.body.userId;
    })

    if(foundAccount){
        userName = foundAccount.userName;
        userId = foundAccount.userId;
        let lastLoginTime = new Date();

        res.cookie("lastLoginTime", lastLoginTime);
        res.cookie("userId", userId);
        res.cookie("userName", userName);

        res.json({
            "userName": foundAccount.userName,
            "userId": foundAccount.userId,
            "lastLoginTime": lastLoginTime
        })
    }else{
        res.send("the account is not found");
    }
})

app.get("/fake-li/account/last-login-time", (req, res)=>{
    res.json({
        "userId": req.cookies.userId,
        "lastLoginTime": req.cookies.lastLoginTime
    })
})

app.get("/fake-li/account/comments", (req, res)=>{
    let cookiesUserId = parseInt(req.cookies.userId);
    
    let foundComments = db.comments.filter((comment)=>{
        return comment.author === cookiesUserId
    })

    let jsonArray = [];
    for(let i = 0; i < foundComments.length; i++){
        jsonArray.push(foundComments[i].content)
    }
    res.json({
        theUserAllComments: jsonArray
    })
})

app.all("*", (req, res)=>{
    res.json({
        "error": 404
    })
})

app.listen(port, ()=>{
    console.log(`Example app listening at http://localhost:${port}`)
})