const member = {
  name:"gary",
  password:"hashed password"
}

const service = {
  serviceOwner:"owner's name",
  name:"service name",
  description:"description of service",
  availability:true,
  bookedBy:"name of member",
  pictures:["image 1 path","image 2 path"],
  likeCount:1,
  comments:[{
    content:"comment",
    datetime:"01/01/2021"
  }]
} 

const db = {  
    member:[
    ], 
    service:[                                   
    ]
};

module.exports = db;