const express = require("express");
const mysql = require("mysql2");
const app = express();
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const multer = require("multer");
const { ReturnDocument } = require("mongodb");
const upload = multer({dest:"upload-files/"});
let pool = null;

app.use(express.json());

//q4
app.use("/member", (req,res,next)=>{
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, "secret", (err, decode)=>{
        if(!err){
            req.member = decode;
            next();
        }else{
            res.json({
                "message":"token invalid"
            })
        }
    });
});
//q1
app.post("/register", async(req, res)=>{
    try{
        const name = req.body.userName;
        const password = req.body.password;
        const hashPassword = await bcrypt.hash(password, 5);

        const [row, fields] = await pool.execute(
            "insert into member (name, password) values(?, ?)",[name, hashPassword]
        );
        res.json(row)
    }catch(error){ 
        res.json(error);
    }
});
//q2
app.post("/login", async (req, res)=>{
    try{
        const memberId = req.body.memberId;
        const password = req.body.password;
        
        const [row,fields] = await pool.execute(
            "select id, name, password from member where id=?",[memberId]
        );
        const result = await bcrypt.compare(password, row[0].password);

        if(result){
            const payload = {
                "id": row[0].id,
            };
            const token = await jwt.sign(payload, "secret");
            return res.json({
                success:true,
                "token": token
            });
        }else{
            return res.json({
                "message": "password is wrong, please try again"
            });
        }
    }catch(error){
        res.json(error);
    }
})
//q5
app.get("/service", async(req, res)=>{
    try{
        const [row, fields] = await pool.execute(
            "select * from service where display = ?",[true]
            );
        if(row[0]){
            res.json(row);
        }else{
            res.json({
                "message": "can't find any services acceptable for registration"
            })
        }
    }catch(error){
        res.json(error);
    }
});
//q6
app.get("/search", async(req, res)=>{
    try{
        const searchItem = req.query.name;
        const [row, fields] = await pool.execute(
            "select * from service where (serviceName like ? or description like ? and display = ?)", [` %${ searchItem }% `, ` %${ searchItem}% `, true]
            );
        if(row){
            res.json(row)
        }else{
            res.json({"message": "this service is not found"})
        }
    }catch(error){
        res.json(error);
    }
})
//q7
app.get("/service/:serviceId", async(req, res)=>{
    try{
        const serviceId = req.params.serviceId;
        const [row, fields] = await pool.execute(
            "select * from service where id = ? and display = ?", [serviceId, true]
            );
        if(row[0]){
            res.json(row);
        }else{
            res.json({"message": "can't find any services acceptable for registration"})
        }
    }catch(error){
        res.json(error);
    }
})
//q8
app.post("/member/service", upload.array("picture"), async(req, res)=>{
    try{
        const memberId = req.member.id;
        const serviceName = req.body.serviceName;
        const description = req.body.serviceDescription;
        const pictureUrl = req.file.path;
        // const picture = req.files;
        // console.log(picture[0].path,picture[1].path,picture[2].path)
        const [row1, fields1] = await pool.execute(
            `insert into service (ownerId, serviceName, description, availability, likeCount, display) 
                values(?,?,?,?,?,?)`,[memberId, serviceName, description, true, 0, true]
        );
        
        const serviceId = row1.insertId;
        const [row2, fields2] = await pool.execute(
            `insert into picture (url, serviceId) values(?,?)`,[pictureUrl, serviceId]
        );
        res.json(row2);
        
    }catch(error){
        res.json(error);
    }
});
//q9
app.patch("/member/service/:serviceId/update", async(req, res)=>{
    try{
        const serviceId = req.params.serviceId;
        const memberId = req.member.id;
        const serviceName = req.body.serviceName;
        const description = req.body.description;
        const display = req.body.display;
           
        const [row, fields] = await pool.execute(
            `select * from service where id = ? and display = ?`,[serviceId, true]
        );
        console.log(row)
        if(row[0]){
            if(row[0].ownerId === memberId){
                const [row2, fields2] = await pool.execute(
                    `update service
                        set serviceName = ?, description = ?, display = ?
                            where id = ? `,[serviceName, description, display, row[0].id]
                );
                res.json(row2)
                
            }else{
                res.json({"message": "you are not this service owner, so can't be edited"});
            }
        }else{
            res.json({"message": "this service is not found"})
        }
    }catch(error){
        res.json(error);
    }
    
})
//q10
app.delete("/member/service/:serviceId/delete", async(req, res)=>{
    try{
        const serviceId = req.params.serviceId;
        const memberId = req.member.id;
        
        const [row, fields] = await pool.execute(
            `select * from service where id=? and display = ?`,[serviceId, true]
        );
    
        if(row[0]){
            if(row[0].ownerId === memberId){
                if(row[0].availability === 1){
                    const [row2, fields2] = await pool.execute(
                        `update service 
                            set display = ? where id = ?`,[false, serviceId]
                    );
                    const [row3, fields3] = await pool.execute(
                        `delete from picture where serviceId = ?`,[serviceId]
                    );
                    res.json(row2)
                }else{
                    res.json({"message": "the service currently booked by member, so the service can't be deleted"})
                }
            }else{
                res.json({"message": "you are not this service owner, so you cant delete"})
            }
        }else{
            res.json({"message": "this service is not found"});
        }
    }catch(error){
        res.json(error);
    }
    
})
//11
app.post("/member/service/:serviceId/like", async(req, res)=>{
    try{
        const serviceId = req.params.serviceId;
        const memberId = req.member.id;
    
        const [row, fields] = await pool.execute(
            `select * from service where id=? and display = ?`,[serviceId, true]
        );
       
        if(row[0]){
            if(row[0].ownerId !== memberId){
                const [row2, fields2] = await pool.execute(
                    `update service set likeCount = ? 
                        where id = ?`,[row[0].likeCount+1, serviceId]
                );
                res.json(row2)
            }else{
                res.json({"message": "you are this service owner, so you cant like this service"});
            }
        }else{
            res.json({"message": "this service is not found"});
        }
    }catch(error){
        res.json(error);
    }
})
//q12
app.post("/member/service/:serviceId/comment", async(req, res)=>{
    try{
        const serviceId = req.params.serviceId;
        const memberId = req.member.id;
        const content = req.body.comment;
        const dateOfCreation = new Date();

        const[row, fields] = await pool.execute(
            `select * from service where id =? and display = ?`,[serviceId, true]
        );

        if(row[0]){
            const [row2, fields2] = await pool.execute(
                `insert into comment (authorId, content, dateOfCreation, serviceId)
                    values(?, ?, ?, ?)`,[memberId, content, dateOfCreation, row[0].id]
            );
            res.send(row2)
        }else{
            res.json({"message": "this service is not found"});
        }
    }catch(error){
        res.json(error);
    }
    
})
//q13
app.post("/member/service/:serviceId/booking", async(req, res)=>{
    try{
        const serviceId = req.params.serviceId;
        const memberId = req.member.id;
        const bookingDate = new Date();

        const[row, fields] = await pool.execute(
            `select * from service where id=? and display=?`,[serviceId, true]
        );

        if(row[0]){
            if(row[0].availability === 1){
                if(row[0].ownerId !== memberId){
                    const [row2, fields2] = await pool.execute(
                        `update service set availability = ? where id = ?`,[false, row[0].id]
                    )
                    const [row3, fields3] = await pool.execute(
                        `insert into booking_record(bookerId, serviceId, bookingDate)
                            values(?,?,?)`,[memberId, row[0].id, bookingDate]
                    );
                    res.json(row3)
                }else{
                    res.json({"message": "you are this service owner, so you cant book this service"});
                }
            }else{
                res.json({"message": "This service has already been reserved by someone else, so you cant book this service"});
            }
        }else{
            res.json({"message": "this service is not found"});
        }
    }catch(error){
        res.json(error);
    }
   
})
//q14
app.delete("/member/service/:serviceId/cancel", async(req, res)=>{
    try{
        const serviceId = req.params.serviceId;
        const memberId = req.member.id;

        const[row, fields] = await pool.execute(
            `select * from service where id=? and display=?`,[serviceId, true]
        );

        if(row[0]){
            if(row[0].ownerId === memberId){
                const [row2, fields2] = await pool.execute(
                    `update service set availability = ? where id = ?`,[true, row[0].id]
                )
                res.send(row2);
            }else{
                res.json({"message": "you are not this service owner, so you cant cancel the booking"});
            }
        }else{
            res.json({"message": "this service is not found"});
        }
    }catch(error){
        res.json(error);
    }
})


app.listen(3080, () => {
	console.log("Example app listening on port 3080!");
	const mysqlPool = mysql.createPool({
		host: 'localhost',
		port: 3306,
		user: 'root', 
		password: 'root',
		database: 'service_provive_db'
	});
	pool = mysqlPool.promise();
});

