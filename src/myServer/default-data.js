const threads = [
    {
        id:1,
        title:"今晚打老虎?",
        createDate:new Date('2021-01-25'),
        category:1
    },
    {
        id:2,
        title:"PUT同PATCH有乜分別?",
        createDate:new Date('2021-01-26'),
        category:2
    },
    {
        id:3,
        title:"咩係idempotent?",
        createDate:new Date('2021-01-27'),
        category:1
    },
]

const comments = [
    {
        id:8899,
        threadId:1,
        content:"就今晚!好唔好!",
        author:886
    },
    {
        id:9000,
        threadId:2,
        content:"PUT係idempotent",
        author:107
    },
    {
        id:18000,
        threadId:2,
        content:"PATCH可以唔係idempotent",
        author:107
    },
    {
        id:12000,
        threadId:3,
        content:"idempotent姐係行一次/十萬次都係同一個結果",
        author:107
    },

]

const accounts = [
    {
        userName:"中國古拳法",
        userId:886
    },
    {
        userName:"陳奕迅間看地球",
        userId:107
    },

]

module.exports = {
    threads: threads,
    comments: comments,
    accounts: accounts
}