const express = require('express');
const cookiesParser = require("cookie-parser");
const multer = require('multer');
const upload = multer({dest:'uploaded-files/'});
const app = express();
const port = 4000;

app.use(express.json());
app.use(cookiesParser());

app.get('/', (req, res) => {
  res.send('Hello  World!');
})

app.get("/factorial/:num", (req, res)=>{
    let number = req.params.num;
    let result = number * 3
    
    res.send(`the result is ${result}`);
})

app.get("/string-handle/name/:name1/surname/:name2", (req, res)=>{
    let firstName = req.params.name1;
    let lastName = req.params.name2;
    res.send(`Hi ${firstName + lastName}, I am happy to see you.`)
})

// const router = express.Router();
// app.use("/math", router);

// router.get("/factorial/:number", (req, res)=>{
//     res.send(req.params.number);
// })

// router.get("/power/:base/:exponent", (req, res)=>{
//     res.send(req.params.base+req.params.exponent);
// })

// const router2 = express.Router();
// app.use("/classroom", router2);

// router2.get("/book/:date", (req, res)=>{
//     res.send(req.params.date)
// })

// router2.get("/availability/:date", (req, res)=>{
//     res.send(req.params.date);
// })

// app.use("/users", (req, res, next)=>{
//     let time = new Date();
//     // req.time = time;
//     res.send(time)
//     next();
// })

// app.get("/users/:userId", (req, res)=>{
//     res.send(req.time)
// })

// app.use("/abc", (req, res, next)=>{
//     let abc = req.method;
//     next();
// })

// app.get("/abc/:123", (req, res)=>{
//     res.send(req.method)
// })

// app.all("*", (req, res)=>{
//     res.send("not found")
// })

// app.use("/static-file", express.static("public"))

// app.use(express.json());
// // app.post('/users', (req, res) => {
// //   console.log(req.body);
// // 	res.send('you can now see the json!');
// // })

app.post('/users', upload.single('picture'), (req, res) => {
	console.log(req.file.originalname, req.body);
	res.send('you can see the file and data!');
})

// app.use(cookiesParser());
// app.post('/users', (req, res) => {
// 	console.log(req.cookies);
// 	res.send('you can see the cookies!');
// })

// app.get("/courses", (req, res)=>{
//   console.log(req.query);
//   res.send("searching full time courses including javascript")
// })

// app.get("/courses", (req, res)=>{
//     let a = req.query.time;
//     let b = req.query.skill;
//     res.cookie("time", a);
//     res.cookie("skill", b);
//     res.send("mm");
// })

app.post("/login", (req, res)=>{
    let username = req.body.username;
    let pw = req.body.password;
    console.log(username, pw);

    if(username === "cin" && pw === "bb"){
        res.cookie("token", "tbb");
    }
    res.send("f")
})

app.post("/private-date", (req, res)=>{
    let a = req.cookies.token;
    console.log(a);
    if(a !== "tbb"){
        res.status(401).end();
        return;
    }else{
        res.send( "My secret, I love cinbb");
    }
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})