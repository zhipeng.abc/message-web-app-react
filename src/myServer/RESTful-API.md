POST /staffs/register
POST /staffs/login

POST /esport/players/profile

POST /esport/teams
POST /esport/teams/members

POST /competition
POST /competition/:compId/teams/register

PATCH /competition/:compId/teams/:teamId

Delete /competition/:compId/teams/:teamId

GET /competition/:compId/report