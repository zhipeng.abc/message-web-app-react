const express = require ("express");
const { MongoClient } = require("mongodb");
const app = express();
const uri = "mongodb://localhost:27017";
const client = new MongoClient(uri);
const { ObjectId } = require("mongodb")
let db = null;


async function initizeDB(){
    try{
        await client.connect();
        db = await client.db("abc2");
    }catch(error){
        console.log(error);
    }
}

app.get("/", async(req, res)=>{
    const cursor = db.collection("shop").find({"_id":"TST"})
    const docs = await cursor.toArray();
    res.json(docs)
})

app.get("/doremi", async(req, res)=>{
    const newDoc = {name: "春風DoReMi", birthday: new Date("1990-07-30"), spell:"比利卡比利那那 撲撲利那比比路多" };
    const result = await db.collection("fasola").insertOne(newDoc);
    res.json(result)
})

app.get("/fasola", async(req, res)=>{
    const newDoc = [{name: "藤原羽月", birthday: new Date("1991-02-14"), spell:"比利卡比利那那 撲撲利那比比路多" },
    { name: "妹尾愛子", birthday: new Date("1990-11-14"), spell:"巴美露古拉露古 拉里羅利卜布" },
    { name: "瀨川音符", birthday: new Date("1991-03-03"), spell:"派派幫幫 普哇普哇普" }
    ];
    const result = await db.collection("fasola").insertMany(newDoc);
    res.json(result)
})


app.get("/all", async(req, res)=>{
    const cursor = db.collection("fasola").find({"name":"藤原羽月"});
    const docs = await cursor.toArray();
    res.json(docs);
})

app.get("/born", async(req, res)=>{
    const query = {birthday: new Date("1990-07-30")}
    const cursor = await db.collection("fasola").findOne(query);
    res.json(cursor);
})

app.get("/add-female", async(req, res)=>{
    const query = {};
    const update = {
        $set: {gender: "female"}
    };
    const cursor = await db.collection("doremi").updateMany(query, update);
    res.json(cursor);
})

app.get("/delete", async(req, res)=>{
    const query = {"gender": "female"};
    const cursor = await db.collection("doremi").deleteMany(query);
    res.json(cursor);
})

initizeDB().then(()=>{
    app.listen(5000,()=>{
        console.log("Example app listening on port 5000!")
    });
}).catch(console.error);
