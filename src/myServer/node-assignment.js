const express = require("express");
const db = require("./service-object");
const app = express();
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const { member, service } = require("./service-object");
const multer = require("multer")
const upload = multer({dest:"upload-files/"})
const port = 4200;


app.use(express.json());

app.use("/login/member", (req, res, next)=>{
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, "secret", (err, decode)=>{
        if(!err){
            req.member = decode;
            next(); 
        }else{
            res.json({
                "message":"token invalid"
            });
        }  
    })
})

//q1
app.post("/register", (req, res)=>{
    let userName = req.body.username;
    let password = req.body.password;

    const foundMember = db.member.find((member)=>{
        return member.name === userName;
    })

    if(foundMember){
        res.json({"message": `sorry, user name ${userName} is be used`})
    }else{
        bcrypt.hash(password, 3).then((hash)=>{
            db.member.push({
                name: userName,
                password: hash
            })
            return res.json({"message": `welcome ${userName}`})
        }) 
    }
})

//q2
app.post("/login", (req, res)=>{
    const foundMember =  db.member.find((member)=>{
        return member.name === req.body.name;
    });
    if(foundMember){
        bcrypt.compare(req.body.password, foundMember.password).then((result)=>{
            if(result){ 
                const payload = { 
                    "name": foundMember.name
                };
                const token = jwt.sign(payload, "secret");
                return res.json({
                    success: true,
                    token: token
                });
            }else{
                return res.json({"message": "password is wrong, please try again"})
            }
        });
    }else{
        return res.json({"message": `username ${req.body.name} is not found, you should register first`})
    }
})

//q5

app.get("/service/available", (req, res)=>{
    const foundService = db.service.filter((service)=>{
        return service.availability === true;
    });
    res.json(foundService);
})

//q6
app.get("/search", (req, res)=>{
    const searchItem = req.query.name;
    const foundService = db.service.filter((service)=>{
        return service.name === searchItem;
    })
    res.json(foundService)
})

//q7
app.get("/service/:serviceName", (req, res)=>{
    const serviceName = req.params.serviceName;

    const foundService = db.service.find((service)=>{
        return serviceName === service.name;
    })

    res.json(foundService);
})

//q8
// POST /service
// POST /member/service
app.post("/login/member/create-service", upload.single("picture"), (req, res)=>{
    const memberName = req.member.name;
    const name = req.body.serviceName;
    const description = req.body.serviceDescription;
    const pictureUrl = req.file.path;
    console.log(req.file);

    const foundMember = db.member.find((member)=>{
        return memberName === member.name; 
    });
    
    if(foundMember){
        db.service.push({ 
            serviceOwner: memberName,
            name: name, 
            description: description,
            availability: true,     
            bookedBy: "",  
            pictures: [pictureUrl],
            likeCount: 0,
            comments:[]
        })
        res.json(db.service[service.length-1])
    }else{
        res.json({"message": "you are not our member"})
    } 
})

//q9
app.patch("/login/member/update-service/:serviceName", (req, res)=>{
    const memberName = req.member.name;
    const serviceName = req.params.serviceName;
    const newDescription = req.body.editDescription; 
    console.log(serviceName)

    const foundMember = db.member.find((member)=>{
        return memberName === member.name; 
    })  

    if(foundMember){
        const foundService = db.service.find((service)=>{
            return serviceName === service.name;
        });
        if(foundService){
            if(foundService.serviceOwner === memberName){
                foundService.description = newDescription; 
                res.json(foundService);
            }else{ 
                res.json({"message": "you are not this service owner, so can't be edited"})
            } 

        }else{
            res.json({"message": "this service is not found"})
        }
    }else{
        res.json({"message": "you are not our member"})
    }
})

//10
app.delete("/login/member/service/:serviceName", (req, res)=>{
    const memberName = req.member.name;
    const serviceName = req.params.serviceName;

    const foundMember = db.member.find((member)=>{ 
        return memberName === member.name;
    }) 
   
    if(foundMember){
        const foundService = db.service.find((service)=>{
            return serviceName === service.name;
        });

        if(foundService){ 
            if(foundService.serviceOwner === memberName){
                db.service.splice(db.service.indexOf(foundService), 1)
                res.json(db.service);
            }else{
                res.json({"message": "you are not this service owner, so you cant delete"})
            }
        }else{
            res.json({"message": "this service is not found"})
        }
    }else{
        res.json({"message": "you are not our member"})
    }
})

//q11
// POST /service/:serviceName/like
// POST /login/member/service/:serviceName/like

// /login/member/like-service/:serviceName
// /login/member/create-service/:serviceName
// /login/member/update-service/:serviceName

app.post("/login/member/like-service/:serviceName", (req, res)=>{
    const memberName = req.member.name;
    const serviceName = req.params.serviceName;

    const foundMember = member.find((member)=>{
        return memberName === member.name; 
    })

    if(foundMember){
        const foundService = db.service.find((service)=>{
            return serviceName === service.name;
        });
        if(foundService){
            if(foundService.serviceOwner !== memberName){
                foundService.likeCount++;
                res.json(foundService);
            }else{
                res.json({"message": "the service is created by you, so you can't like the service"})
            }

        }else{
            res.json({"message": "the service is not found"})
        }
    }else{
        res.json({"message": "you are not member"})
    }
})  
 
//q12
app.post("/login/member/service/:serviceName/comment", (req, res)=>{
    const memberName = req.member.name;
    const serviceName = req.params.serviceName;
    const content = req.body.content; 
    const date = new Date();
    
    const foundMember = member.find((member)=>{
        return memberName === member.name;
    });

    if(foundMember){
        const foundService = db.service.find((service)=>{
            return serviceName === service.name;
        });

        if(foundService){
            foundService.comments.push({"content": content, "date": date});
            res.json(foundService);
        }else{
            res.json({"message": "the service is not found"});
        }
    }else{
        res.json({"message": "you are not member"})
    }

})

//q13
app.post("/login/member/book-service/:serviceName", (req, res)=>{
    const memberName = req.member.name;
    const serviceName = req.params.serviceName; 

    const foundMember = member.find((member)=>{
        return memberName === member.name;
    });
    
    if(foundMember){
        const foundService = db.service.find((service)=>{
            return serviceName === service.name;
        });

        if(foundService){
            if(foundService.serviceOwner !== memberName){
                foundService.availability = false;
                foundService.bookedBy = memberName;
                res.json(foundService);
            }else{
                res.json({"message": "the service is created by you, so you can't book the service"});
            }
        }else{
            res.json({"message": "the service is not found"});
        }
    }else{
        res.json({"message": "you are not member"})
    }
    
})

//q14
app.patch("/login/member/remove-booking/:serviceName", (req, res)=>{
    const memberName = req.member.name;
    const serviceName = req.params.serviceName;
   
    const foundMember = db.member.find((member)=>{ 
        return memberName === member.name;
    }); 
    
    if(foundMember){  
        const foundService = db.service.find((service)=>{
            return serviceName === service.name; 
        });
        
        if(foundService){
            if(foundService.serviceOwner === memberName){
                foundService.availability = true;
                foundService.bookedBy = "";
                res.json(foundService);
            }else{
            res.json({"message": "if you want to cancel the booking, please contact the service owner"});
            }
        }else{
            res.json({"message": "the service is not found"});
        }
    }else{
        res.json({"message": "you are not member"}) 
    }
})

app.listen(port, ()=>{
    console.log(`Example app listening at http://localhost:${port}`)
})