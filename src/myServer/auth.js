const express = require("express");
const app = express();
const port = 3400;
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const cookieParser = require("cookie-parser");

app.use(express.json());
app.use(cookieParser());

const saltRounds = 3;


app.post("/register", (req, res) => {
    let username = req.body.username;
    let password = req.body.password;
    let registrationTime = new Date();

    bcrypt.hash(password, saltRounds).then((hash) => {
        db.users.push({
            username: username,
            password: hash
        })
        return res.json(db)
    })
})

const db = {
    users: []
}

app.post("/login", (req, res) => {
    const userInDatabase = db.users.find((user) => {
        return user.username === req.body.username;
    });

    if (userInDatabase) {
        bcrypt.compare(req.body.password, userInDatabase.password).then((result) => {
            if (result) {
                const payload = {
                    "username": userInDatabase.username,
                };
                const token = jwt.sign(payload, "secret");

                return res.json({
                    success: true,
                    token: token
                });
            } else {
                return res.json({
                    success: false
                })
            }
        });

    } else {
        return res.json({
            success: false
        })
    }
})

app.post("/secret-data", (req, res)=>{
    const token = req.body.token;
    jwt.verify(token, "secret", (err, decoded)=>{
        if(err){
            return res.json({message: "hacker!!"});
        }else{
            const username = decoded.username;
            return res.json({message: username+" gogogo"})
        }
    })
})


app.post("/public-resource", (req, res) => {

})

app.post("/resource", (req, res) => {

})

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})


// 1. route
// 2. extract user name password
// 3. save username hased pw on server 
// 4. generate token
// 5. res token