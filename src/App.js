import './Sass/App.scss';
import { useState, useEffect } from "react";
import { saveMySocket, submitLogin, submitRegister, fetchList, submitLogout, searchMember, fetchMessage, connectSocket, sendSocketMessage, submitTurnToRegister, clearConversation, firstSend } from "./actions";
// import { useParams, useRouteMatch } from "react-router-dom";
import { connect } from "react-redux";
// import { io } from "socket.io-client";
// import reactDom from 'react-dom';
// import { Socket } from 'socket.io-client';


function ConversationContent(props) {
	const [inputText, setInputText] = useState("");
	const token = localStorage.getItem("token");
	const memberId = parseInt(localStorage.getItem("memberId"));
	const senderId = props.senderIdForConversationContent;
	const receiverId = props.receiverIdForConversationContent;

	const handleInputText = (e) => {
		setInputText(e.target.value)
	}

	const conversationsFromServer = props.conversationsFromServer;

	const handleSendMessage = () => {
		let member2Id = null;
		if (memberId === receiverId) {
			member2Id = senderId;
			props.dispatch(sendSocketMessage(inputText, token, memberId, member2Id));
		} else if (memberId === senderId) {
			member2Id = receiverId;
			props.dispatch(sendSocketMessage(inputText, token, memberId, member2Id));
		}
	}

	const friendId = () => {
		if (memberId === receiverId) {
			return senderId;
		} else if (memberId === senderId) {
			return receiverId;
		}
	}
console.log(conversationsFromServer)
	return (
		<div className="mainConversation">
			<div>Friend ID: {friendId()}</div>
			<div className="conversationWindow">
				{conversationsFromServer.map((elem) => {
					return (
						<div key={elem.message_id+elem.content} className="conversationList">
							{
								elem.sender_id === memberId ?<div className="rightMessage">{elem.content}</div>	: <div className="leftMessage">{elem.content}</div>
							}
						</div>)
				})}
			</div>
			<div className="conversationSend">
				<input type="text" value={inputText} onChange={handleInputText} />
				<button onClick={handleSendMessage}>send</button>
			</div>
		</div>
	)
}

function ConversationBox(props) {
	const token = localStorage.getItem("token");
	
	const conversationId = props.conversationId;

	const handleShowConversation = () => {
		console.log(token, conversationId, props.senderId, props.receiverId)
		props.dispatch(fetchMessage(token, conversationId, props.senderId, props.receiverId));
	}

	const friendId = ()=>{
		if(parseInt(localStorage.getItem("memberId")) === props.receiverId){
			return props.senderId
		}else if(parseInt(localStorage.getItem("memberId")) === props.senderId){
			return props.receiverId
		}
	}
	return (
		<div onClick={handleShowConversation} className="conversationBox">
			<div>Friend ID: {friendId()}</div>
			<div className="content">{props.content}</div>
			<div>{props.messageTime.slice(0, 10) + " " + props.messageTime.slice(11, 19)}</div>
		</div>

		
	)
}


function ConversationList(props) {
	const token = localStorage.getItem("token");

	useEffect(() => {
		props.dispatch(fetchList(token));
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const friendList = props.friendList;

	return (
		<div >
			{friendList.map((elem, index) => {
				return <ConversationBox key={elem + index} content={elem.content} messageTime={elem.message_time} senderId={elem.sender_id} receiverId={elem.receiver_id} conversationId={elem.conversation_id} {...props} />
			})}
		</div>
	)
}


function Search(props) {
	const [inputMemberId, setInputMemberId] = useState("");

	const handleInputMemberId = (e) => {
		setInputMemberId(e.target.value)
	}

	const handleSearch = () => {
		props.dispatch(searchMember(inputMemberId))
		
	}
	const foundMember = props.foundMember;
	const foundMemberSuccessMessage = props.foundMemberSuccessMessage;
	const foundMemberFailMessage = props.foundMemberFailMessage;
	const token = localStorage.getItem("token");


	const memberId = parseInt(localStorage.getItem("memberId"));

	const senderId = memberId;
	const receiverId = foundMember.member_id;
	const handleSendMessage = () => {
		props.dispatch(clearConversation());
		props.dispatch(firstSend(senderId, receiverId))
		props.dispatch(sendSocketMessage("hi", token, senderId, receiverId));
		setTimeout(function(){
			props.dispatch(fetchList(token))
		},1000)
	}

	const handleLogout = () => {
		localStorage.removeItem("token");
		localStorage.removeItem("memberId");
		props.dispatch(submitLogout())
	}

	return (
		<div className="searchPanel">
			<input value={inputMemberId} onChange={handleInputMemberId}  />
			<button onClick={handleSearch}>Search</button>
			<button onClick={handleLogout}>Logout</button>
			<div onClick={handleSendMessage} className="addFriend">
				<div>{foundMember.length !== 0 ? <div>{foundMember.member_id}</div> : null}</div>
				<div>{foundMemberSuccessMessage ? <div>{foundMemberSuccessMessage}</div> : null}</div>
				<div>{foundMemberFailMessage ? <div>{foundMemberFailMessage}</div> : null}</div>
			</div>
		</div>
	)
}

function MainList(props) {
	return (
		<div className="mainList">
			<Search {...props} /><br />
			<ConversationList {...props} />
		</div>
	)
}

function Chatroom(props) {
	
	const memberId = localStorage.getItem("memberId");

	useEffect(() => {
		props.dispatch(saveMySocket(memberId));
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	return (
		<div className="chatroom">
				<MainList {...props} />
				<ConversationContent {...props} />
		</div>
	)
}

function Register(props) {
	const [inputMemberId, setInputMemberId] = useState("");
	const [inputPassword, setInputPassword] = useState("");

	const handleInputMemberId = (e) => {
		setInputMemberId(e.target.value);
	}

	const handleInputPassword = (e) => {
		setInputPassword(e.target.value);
	}

	const handleRegister = () => {
		props.dispatch(submitRegister(inputMemberId, inputPassword));
	}


	return (
		<div className="registerPanel">
			<input type="text" value={inputMemberId} onChange={handleInputMemberId} placeholder="Your New Member ID" />
			<input type="password" value={inputPassword} onChange={handleInputPassword} placeholder="Your New Password" />
			<button onClick={handleRegister}>Register</button>
			<div>{props.registerFailMessage}</div>
		</div>
	)
}

function Login(props) {
	const [inputMemberId, setInputMemberId] = useState("");
	const [inputPassword, setInputPassword] = useState("");

	const handleInputMemberId = (e) => {
		setInputMemberId(e.target.value);
	}

	const handleInputPassword = (e) => {
		setInputPassword(e.target.value);
	}

	const handleLogin = () => {
		props.dispatch(submitLogin(inputMemberId, inputPassword))
	}

	const handleTurnToRegister = () =>{
		props.dispatch(submitTurnToRegister())
	}

	return (
		<div className="loginPanel">
			<input type="text" value={inputMemberId} onChange={handleInputMemberId} placeholder="Member ID" />
			<input type="password" value={inputPassword} onChange={handleInputPassword} placeholder="Password" />
			<button onClick={handleLogin}>Login</button>
			<p onClick={handleTurnToRegister}>Please click here to apply your account</p>
			<div>{props.loginFailMessage}</div>
		</div>
	)
}



function App(props) {
	useEffect(() => {
		props.dispatch(connectSocket());
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [])


	if (props.memberId) {
		localStorage.setItem("memberId", props.memberId)
	}
	if (props.token) {
		localStorage.setItem("token", props.token)
	}
	const token = localStorage.getItem("token")

	if (!props.isSocketConnected) {
		return <div>Loading</div>
	}

	if (token) {
		return (
			<Chatroom {...props} />
		)
	} else {
		return (
			<div className="index">
				<h1>Message Web App</h1>
				{props.turnToRegister? null : <><Login {...props} /></>}
				{props.turnToRegister? <><Register {...props} /><br /></> : null}
			</div>
		);
	}
}

const mapStateToPros = function (state) {
	const webAppState = state.webApp;
	return {
		isSocketConnected: webAppState.socket !== null,
		memberId: webAppState.memberId,
		member2IdForConversationContent: webAppState.member2IdForConversationContent,
		token: webAppState.token,
		isLogged: webAppState.isLogged,
		friendList: webAppState.friendList,
		foundMember: webAppState.foundMember,
		loginSuccessMessage: webAppState.loginSuccessMessage,
		loginFailMessage: webAppState.loginFailMessage,
		registerSuccessMessage: webAppState.registerSuccessMessage,
		registerFailMessage: webAppState.registerFailMessage,
		foundMemberSuccessMessage: webAppState.foundMemberSuccessMessage,
		foundMemberFailMessage: webAppState.foundMemberFailMessage,
		fetchConversationSuccessMessage: webAppState.fetchConversationSuccessMessage,
		fetchConversationFailMessage: webAppState.fetchConversationFailMessage,
		conversationsFromServer: webAppState.conversationsFromServer,
		senderIdForConversationContent: webAppState.senderIdForConversationContent,
		receiverIdForConversationContent: webAppState.receiverIdForConversationContent,
		turnToRegister: webAppState.turnToRegister,
	}

}

export default connect(mapStateToPros)(App);
